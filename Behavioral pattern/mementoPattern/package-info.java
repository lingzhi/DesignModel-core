/**
 * @author lingzhi 备忘录模式使用三个类 Memento、Originator 和 CareTaker。Memento
 *         包含了要被恢复的对象的状态。Originator 创建并在 Memento 对象中存储状态。Caretaker 对象负责从 Memento
 *         中恢复对象的状态。 MementoPatternDemo，我们的演示类使用 CareTaker 和 Originator
 *         对象来显示对象的状态恢复。
 */
package mementoPattern;