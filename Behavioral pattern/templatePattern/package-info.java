/**
 * @author lingzhi 我们将创建一个定义操作的 Game 抽象类，其中，模板方法设置为 final，这样它就不会被重写。Cricket 和
 *         Football 是扩展了 Game 的实体类，它们重写了抽象类的方法。
 * 
 *         TemplatePatternDemo，我们的演示类使用 Game 来演示模板模式的用法。
 */
package templatePattern;