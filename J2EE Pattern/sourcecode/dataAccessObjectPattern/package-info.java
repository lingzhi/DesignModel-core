/**
 * @author lingzhi 我们将创建一个作为模型对象或数值对象的 Student 对象。StudentDao
 *         是数据访问对象接口。StudentDaoImpl 是实现了数据访问对象接口的实体类。DaoPatternDemo，我们的演示类使用
 *         StudentDao 来演示数据访问对象模式的用法。
 */
package sourcecode.dataAccessObjectPattern;