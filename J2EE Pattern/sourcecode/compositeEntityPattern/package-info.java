/**
 * @author lingzhi 我们将创建作为组合实体的 CompositeEntity 对象。CoarseGrainedObject
 *         是一个包含依赖对象的类。
 * 
 *         CompositeEntityPatternDemo，我们的演示类使用 Client 类来演示组合实体模式的用法。
 */
package sourcecode.compositeEntityPattern;