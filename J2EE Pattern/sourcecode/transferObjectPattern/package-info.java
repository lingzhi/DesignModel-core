/**
 * @author lingzhi 我们将创建一个作为业务对象的 StudentBO 和作为传输对象的 StudentVO，它们都代表了我们的实体。
 * 
 *         TransferObjectPatternDemo，我们的演示类在这里是作为一个客户端，将使用 StudentBO 和 Student
 *         来演示传输对象设计模式。
 */
package sourcecode.transferObjectPattern;