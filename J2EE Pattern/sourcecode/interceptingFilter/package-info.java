/**
 * @author lingzhi 我们将创建 FilterChain、FilterManager、Target、Client
 *         作为表示实体的各种对象。AuthenticationFilter 和 DebugFilter 表示实体过滤器。
 *         InterceptingFilterDemo，我们的演示类使用 Client 来演示拦截过滤器设计模式。
 */
package sourcecode.interceptingFilter;