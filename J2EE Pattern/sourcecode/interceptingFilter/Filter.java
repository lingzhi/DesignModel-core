package sourcecode.interceptingFilter;

public interface Filter {
	public void execute(String request);
}