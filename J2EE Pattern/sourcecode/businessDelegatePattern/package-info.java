/**
 * @author lingzhi 我们将创建
 *         Client、BusinessDelegate、BusinessService、LookUpService、JMSService 和
 *         EJBService 来表示业务代表模式中的各种实体。
 * 
 *         BusinessDelegatePatternDemo，我们的演示类使用 BusinessDelegate 和 Client
 *         来演示业务代表模式的用法。
 */
package sourcecode.businessDelegatePattern;