package sourcecode.businessDelegatePattern;

public interface BusinessService {
	public void doProcessing();
}