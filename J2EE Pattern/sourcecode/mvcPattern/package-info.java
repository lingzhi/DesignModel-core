/**
 * @author lingzhi 我们将创建一个作为模型的 Student 对象。StudentView
 *         是一个把学生详细信息输出到控制台的视图类，StudentController 是负责存储数据到 Student
 *         对象中的控制器类，并相应地更新视图 StudentView。
 * 
 *         MVCPatternDemo，我们的演示类使用 StudentController 来演示 MVC 模式的用法。
 */
package sourcecode.mvcPattern;