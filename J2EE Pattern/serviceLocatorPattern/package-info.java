/**
 * @author lingzhi 我们将创建 ServiceLocator、InitialContext、Cache、Service
 *         作为表示实体的各种对象。Service1 和 Service2 表示实体服务。
 * 
 *         ServiceLocatorPatternDemo，我们的演示类在这里是作为一个客户端，将使用 ServiceLocator
 *         来演示服务定位器设计模式。
 */
package serviceLocatorPattern;