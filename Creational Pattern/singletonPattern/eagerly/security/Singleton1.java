package singletonPattern.eagerly.security;

/**
 * 这种方式采用双锁机制，安全且在多线程情况下能保持高性能。
 *	getInstance() 的性能对应用程序很关键。
 * @author lingzhi
 *
 */
public class Singleton1 {
	private volatile static Singleton1 singleton;

	private Singleton1() {
	}

	public static Singleton1 getSingleton() {
		if (singleton == null) {
			synchronized (Singleton1.class) {
				if (singleton == null) {
					singleton = new Singleton1();
				}
			}
		}
		return singleton;
	}
}