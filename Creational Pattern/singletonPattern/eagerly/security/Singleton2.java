package singletonPattern.eagerly.security;

/**
 * 这种方式能达到双检锁方式一样的功效，但实现更简单。
 * 对静态域使用延迟初始化，应使用这种方式而不是双检锁方式。
 * 这种方式只适用于静态域的情况，双检锁方式可在实例域需要延迟初始化时使用。
 * @author lingzhi
 *
 */
public class Singleton2 {
	private static class SingletonHolder {
		private static final Singleton2 INSTANCE = new Singleton2();
	}

	private Singleton2() {
	}

	public static final Singleton2 getInstance() {
		return SingletonHolder.INSTANCE;
	}
}