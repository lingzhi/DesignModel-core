package sourcecode.FactoryPattern;

public interface Shape {
	void draw();
}