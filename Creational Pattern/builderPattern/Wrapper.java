package builderPattern;

/**
 * 纸装
 * @author lingzhi
 *
 */
public class Wrapper implements Packing {

	@Override
	public String pack() {
		return "Wrapper";
	}
}