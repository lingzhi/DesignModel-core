package builderPattern;

/**
 * 可乐	
 * @author lingzhi
 *
 */
public class Coke extends ColdDrink {

	@Override
	public float price() {
		return 30.0f;
	}

	@Override
	public String name() {
		return "Coke";
	}
}