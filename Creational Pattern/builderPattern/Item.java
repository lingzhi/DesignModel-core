package builderPattern;

/**
 * 套装
 * @author lingzhi
 *
 */
public interface Item {
	public String name();

	public Packing packing();

	public float price();
}