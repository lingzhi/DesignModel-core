package builderPattern;

/**
 * 瓶装
 * @author lingzhi
 *
 */
public class Bottle implements Packing {

	@Override
	public String pack() {
		return "Bottle";
	}
}