package builderPattern;

/**
 * 素食汉堡
 * @author lingzhi
 *
 */
public class VegBurger extends Burger {

	@Override
	public float price() {
		return 25.0f;
	}

	@Override
	public String name() {
		return "Veg Burger";
	}
}