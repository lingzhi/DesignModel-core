package builderPattern;

/**
 * 汉堡包
 * @author lingzhi
 *
 */
public abstract class Burger implements Item {

	@Override
	public Packing packing() {
		return new Wrapper();
	}

	@Override
	public abstract float price();
}