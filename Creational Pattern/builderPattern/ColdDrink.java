package builderPattern;

/**
 * 冷饮
 * @author lingzhi
 *
 */
public abstract class ColdDrink implements Item {

	@Override
	public Packing packing() {
		return new Bottle();
	}

	@Override
	public abstract float price();
}