package sourcecode.adapter;

/**
 * 跟高级的播放器
 * 
 * @author 1
 *
 */
public interface AdvancedMediaPlayer {
	public void playVlc(String fileName);

	public void playMp4(String fileName);
}
