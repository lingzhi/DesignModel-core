/**
 * @author lingzhi 我们有一个类 Employee，该类被当作组合模型类。CompositePatternDemo，我们的演示类使用
 *         Employee 类来添加部门层次结构，并打印所有员工。
 */
package compositePattern;