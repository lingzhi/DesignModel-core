/**
 * @author lingzhi 我们将创建一个 Person 对象、Criteria 接口和实现了该接口的实体类，来过滤 Person
 *         对象的列表。CriteriaPatternDemo，我们的演示类使用 Criteria 对象，基于各种标准和它们的结合来过滤 Person
 *         对象的列表。
 */
package filterPattern;